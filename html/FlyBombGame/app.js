let rectangleBlue = document.getElementById('rectangleBlue');
let rectanglePink = document.getElementById('rectanglePink');
let cursor = document.getElementById('cursor');


let x = 0, y = 0, dX = 50, dY = 50, dxPink = 700, dyPink = -300;
let color = 0x78a0f1, r=0, g=0, b=0;

window.onmousemove = function(event) {
	x = event.pageX;
	y = event.pageY;
}

setInterval ( function() {
	rectangleBlue.innerHTML = 'Позиция курсора: ' +dX+ '/'+ dY + ', rgb = ' + `rgb(${r},${g},${b})`;
	cursor.style.left = (x-25)+'px';
	cursor.style.top =  (y-25)+'px';
	if ( dX < 700 && dY == 50) { dX += 1, r += 1} 
	if ( dX == 700 && dY < 300) { dY += 1, g += 1}
	if ( dX <= 700 && dY == 300) { dX -= 1, b += 1 }
	if ( dX == 50 && dY >= 50) { dY -= 1, r -= 2, g -= 2, b -= 3 }
	
	if ( dyPink < 250) { dyPink += 1 } 
	else { dyPink -= 1 }
	
	rectangleBlue.style.left = dX+'px';
	rectangleBlue.style.top = dY+'px';
	
	
	rectangleBlue.style.backgroundColor = `rgb(${r},${g},${b})`;
		
	rectanglePink.style.left = dxPink+'px';
	rectanglePink.style.top = dyPink+'px';
	
	
}, 1000/100);