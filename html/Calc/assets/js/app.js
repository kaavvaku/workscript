var number1 = 0 //первый множитель/слагаемое/делимое
var number2 = 0 //второй множитель/слагаемое/делитель
var result //сюда запишем результат выполнения операции
var memory = 0 //переменная для хранения значения в памяти
var flag = '' //флаг, указывает текущую операцию '+','-','*','/','%'
var flag_operation = false //флаг, указывает что есть активная мат.операция, после '=' обнуляется

//функция нажатия на кнопку с цифрой 0-9
function btn_number(val) {
	let elem = document.getElementById('screen'); //получаем переменную поля screen( это наш рабочий экран)
	let log = document.getElementById('screen__log')
	if (elem.value.length > 9) elem.style.fontSize = '16px';
	if (elem.value.length > 14) elem.style.fontSize = '14px';
	if (elem.value.length > 20) return; //ограничение на макс. кол-во символов на экране. не больше 15

	if (elem.value == '0' ) { //если на экране '0', значит нажатая цифра затирает этот 0
		elem.value = val
		log.value = val
	} else {
		elem.value += val //если нет, то добавляем цифру в конец строки на экране
		log.value += val
	}
}

//ф-ия нажатия на кнопку 'clear' очистки всего
function btn_clear() {
	let elem = document.getElementById('screen') //получить переменную основного экрана
	let log = document.getElementById('screen__log')
	let memoryClear = document.getElementById('screen__memory') //получить переменную экрана памяти 
	elem.value = log.value = number1 = number2 = result = 0 //очистить экран, и все значения переменных
	flag = '' //очистить флаги
	flag_operation = false
	memoryClear.value = '' //очистить индикатор данных в памяти с экрана

}

//ф-ия нажатия на кнопку 'backspace' удалить последний символ
function btn_backspace() {
	let elem = document.getElementById('screen')
	let log = document.getElementById('screen__log')
	
	if (elem.value == '0') return 0 //если на экране '0' - ничего не делаем, оставляем '0'

	let shortValue = elem.value //создадим вспомогательную переменную
	shortValue = shortValue.slice(0, -1) //занесем в нее строку с экрана без последнего символа
	if (elem.value.length == '1') { //если на экране только один символ
		elem.value = '0' //то запишем на экран '0'
		log.value = '0'
	} else {
		elem.value = shortValue //иначе, запишем нашу укороченную строку
		log.value = shortValue
	}
}

//ф-ия нажатия на любую кнопку с мат.операцией ('=','+','-','/','*'). Получает в качестве параметра знак мат.операции
function btn_oper(operValue) {
	let elem = document.getElementById('screen')
	let log = document.getElementById('screen__log')
	if (flag_operation) { //если установлен флаг операции, т.е. не после '='. z.b. 5+5-
		let nmb = elem.value //во вспомог. перем. nmb занесем значение строки на экране. т.к. с самой строкой не дает выполнять операции
		number2 = Number(nmb.slice(number1.toString().length + 1)) //отделяем второй операнд от знака операции ( 34+57  -> number2 = 57 )
		if (operValue == '=') {
			switch (flag) {
				case '+':
					result = number1 + number2
					break;
				case '-':
					result = number1 - number2
					break;
				case '*':
					result = number1 * number2
					break;
				case '/':
					result = number1 / number2
					break;
				case '%':
					result = number1 % number2
					break;
			}
			elem.value = result.toString() //выводим на экран результат мат. операции
			log.value = ''   //стерли лог в верхнем окне
			flag_operation = false //сбрасываем флаг активной операции
			flag = '' //сбрасываем флаг операции
		} else {
			writeScreen(elem, log, operValue)
		}
	} else {
		if (operValue == '=') return
		else writeScreenEasy(elem, log, operValue)

	}
}

function resultOperation() {
	switch (flag) {
		case '+':
			return number1 + number2
		case '-':
			return number1 - number2
		case '*':
			return number1 * number2
		case '/':
			return number1 / number2
		case '%':
			return number1 % number2
	}
}

//вспомогательная ф-ия для выполнения мат. операции и вывода результата на экран.
function writeScreen(elem, log, operValue) {
	let res = 0 //вспомогательная переменная, пригодится во вспомогательной ф-ии writeScreen()
	// если строка на экране оканчивается на знак операции, то ничего не делаем, только меняем знак на новый. Иначе проводим операцию через ф-ию resultOperation()
	if (elem.value.slice(-1) !== '/' & elem.value.slice(-1) !== '*' & elem.value.slice(-1) !== '+' & elem.value.slice(-1) !== '-' & elem.value.slice(-1) !== '%') {  
		res = resultOperation()
		number1 = res
		elem.value = res.toString()
		elem.value += operValue
//		log.value += res.toString()
		log.value += operValue
	} else {
		let string = elem.value.split('')
		string[string.length - 1] = operValue
		elem.value = string.join('')
		log.value = string
	}
	flag = operValue  //устанавливаем новый флаг операции 
	flag_operation = true  //также устанавливаем флаг активной операции в true
}

//ф-ия не выполняет мат. операцию, т.к. задан пока только один операнд. Сохраняем его в number1 и выводим на экран знак операции. Устанавливаем флаги.
function writeScreenEasy(elem, log, operValue) {
	number1 = Number(elem.value)
	elem.value += operValue
	log.value += operValue
	flag = operValue
	flag_operation = true
}

function btn_Mplus(result) {
	let elem = document.getElementById('screen__memory')
	elem.value = 'M'
	memory = Number(result)
}

function btn_MS() {
	let elem = document.getElementById('screen')
	elem.style.fontSize = '30px';
	elem.style.color = 'red';
}
