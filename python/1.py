#! /usr/local/bin/python
# -*- coding: utf-8 -*-

def n_in_k(n, k):
	sum = 0
	print("n = " + str(n) + " : k = " + str(k))
	if n <= 20:
		for i in range(1, n):
			if i % 2 == 0:
				sum += i ** k
				print(str(i) + " ^ " + str(k) + " = " + str(i ** k))
		return(sum)
	else:
		return(0)

def sqr(z):
	sm = 0
	arc = [0]
	print("z = " + str(z))
	for i in range(1, z+1):
	#	print(str(i) + " ^ 2 = " + str(i**2))
		print("2 ^ " + str(i) + " = " + str(2**i))


my_list = [15, 10, 8, 5, -1, 2, 1, 0, -4, -7, -10, -15, -30]
word_list = ["one", "two", "tree", "four", "stop", "five", "six", "seven"]

i = 0
my_sum = 0
print(len(my_list))
while i < len(my_list):
	if my_list[i] < 0:
		my_sum += my_list[i]
	i += 1

print("Сумма отрицательных чисел равна: " + str(my_sum))

i = len(word_list)
while i >= 0:
	if word_list[i-1] == "stop":
		break
	print(word_list[i-1])
	i -= 1
