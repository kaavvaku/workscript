#! /usr/local/bin/python
# -*- coding: utf-8 -*-

A = [1,2,3,4,5]
print (A)
A.append(6)
print (A)

B = [x**2 for x in A if x%2==0]
print (B)
B.pop()
print (B)