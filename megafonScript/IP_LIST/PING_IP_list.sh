#!/bin/bash


read -p "Please enter name of file with IP-address: " FileName   # ввод названия файла со списком IP-адресов

cp /etc/null $(pwd)/PingLog_$FileName     # стереть содержимое ( если оно есть ) файла в котором ведется лог ping

while read line  # конструкция чтения из файла ( FileName ) строк ( в переменную line )  
	do	
		# clear
		# echo "ping $line" 
		ping -w 1 -c 3 -i 0.2 $line  > /etc/null 2>>$(pwd)/ErrorLog_$FileName # пинг со временем deadline -w 1с, без вывода на экран
		if (( "$?"=="0" ))   # в цикле если результат выполнения команды пинг успешный $?=0
		 then	echo -e "$line  \t\t [OK]"  >> $(pwd)/PingLog_$FileName
		 else	echo -e "$line  \t\t[FAIL]" >> $(pwd)/PingLog_$FileName
		fi
	done < $FileName   # условие окончания файла
 
