#!/bin/bash
# разделяет IP-адрес на 4 октета, а затем выводит все айпишники 24 сетки в файл 
# clear                

read -p "Please enter Net /24: " Net             # предложение ввести IP-сеть с 24 маской
echo "Net: $Net " 

cp /dev/null $(pwd)/"$Net".txt                   # стираем содержимое файла с IP-адресами ( на всякий случай если он не пустой )

dot_one=0; dot_two=0; dot_three=0;               # создаем переменные, в которые запишем номер символов "." в айпишнике
oct_one=0; oct_two=0; oct_three=0; oct_four=0    # эти переменные будут содержать 4 октета нашего айпишника

dot_one=`expr index "$Net" .`                    # определяем номер первого совпавшего символа . в айпишнике `expr index $string $substring`
oct_one=${Net:0:$dot_one-1}                      # заносим в oct_one первый октет ( в строке Net с 0 символа на длину $dot_one-1  ${String:0:$lengh}

temp=`expr index "${Net:$dot_one}" .`            # аналогично вычисляем второй, третий и четвертый октеты
dot_two=$(($temp+$dot_one))
oct_two=${Net:$dot_one:$dot_two-$dot_one-1}

temp2=`expr index "${Net:$dot_two}" .`
dot_three=$(($temp2+$dot_two))
oct_three=${Net:$dot_two:$dot_three-$dot_two-1}

oct_four=${Net:$dot_three}

for (( i=1;i<=254;i++ )) do                      # в цикле собираем из октетов айпишники, последний октет подставляем от 1 до 254 и выводим в файл
	echo "$oct_one.$oct_two.$oct_three.$i"  >> $(pwd)/"$Net".txt
done

# echo -e "Place of dota        : dot_one - $dot_one \n\t\t\t\t"\  ": dot_two - $dot_two \n\t\t\t\t"\  ": dot_three - $dot_three \n"
# echo -e "Octets               : oct_one    - $oct_one \n\t\t\t\t"\  ": oct_two    - $oct_two \n\t\t\t\t"\  ": oct_three  - $oct_three"
# echo -e "                     : oct_four   - $oct_four"
# echo -e "\n\n"


