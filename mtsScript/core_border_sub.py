#!/ /usr/local/bin/python
# -*- coding: utf-8 -*-

# Этот скрипт создает конфиг для настройки новых sub-interface на новых Core в УЛьяновске 10.222.128.25  и Border 10.222.128.50

# Создадим список требуемых вланов

vlans = [540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,
	670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696]

# Создаем двумерный списка, перый элемент - номер влана, второй элемент - description

#array = [[1,2,3], [4,5,6], [7,8,9]]
#print("array = " + str(array))
#print("array[0][0] = : " + str(array[0][0]))

desc = [[540,"pppoe-test"],[541,"svnet-peski3"],[542,"svnet-peski2"],[543,"."],[544,"."],[545,"svnet-sta10"],[546,"svnet-hsm25"],[547,"svnet-sam21"],
	[548,"svnet-sam11"],[549,"svnet-sho5"],[550,"svnet-kuz29"],[551,"svnet-kam39b"],[552,"svnet-otr70"],[553,"svnet-rja110"],[554,"svnet-peski1"],
	[555,"svnet-omega"],[556,"omega"],[557,"l2tun-svnet"],[558,"svnet-kinda1"],[559,"svnet-kinda2"],[560,"svnet-okt22"],[561,"svnet-pol30"],
	[562,"svnet-pol46"],[563,"svnet-pus54"],[564,"svnet-kor19"],[565,"svnet-var4"],[566,"svnet-hru41"],[567,"svnet-lun16"],[568,"svnet-lt54"],
	[569,"svnet-mir35"],[670,"ntown-liv11"],[671,"ntown-soz48"],[672,"ntown-soz82"],[673,"ntown-fil9"],[674,"ntown-tul28"],[675,"ntown-avs3"],
	[676,"ntown-sur33"],[677,"ntown-tel12"],[678,"ntown-ore44"],[679,"ntown-gog10"],[680,"ntown-dim71"],[681,"ntown-krp13a"],[682,"ntown-zar31"],
	[683,"ntown-brs78"],[684,"ntown-met23"],[685,"."],[686,"ntown-soz26"],[687,"ntown-pen21"],[688,"ntown-drn5"],[689,"ntown-lvo9"],[690,"ntown-uln13"],
	[691,"ntown-uln22"],[692,"ntown-nov16"],[693,"ntown-avs33"],[694,"ntown-sur1"],[695,"ntown-lkm41"],[696,"ntown-liv22"]]



# Создаем переменную f, связанную с файлом, куда будем писать наш конфиг
f = open("core_border_sub.cfg",'w')

# 10.222.128.25  

f.write("****************************************************************************************************************************" + '\n')
f.write("**************************************************** 10.222.128.25 *********************************************************" + '\n')
f.write("****************************************************************************************************************************" + '\n' + '\n'+ '\n')

for k in desc:
	f.write("--------------------------------------  10.222.128.25  Vlan " + str(k) + " ------------------------------" + '\n' + "!" + '\n')
	f.write("interface TenGigE0/0/0/5." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/0/0/5." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/0/0/5." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')
	
	f.write("interface TenGigE0/0/0/6." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/0/0/6." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/0/0/6." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')

	f.write("interface TenGigE0/0/0/16." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/0/0/16." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/0/0/16." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')

	f.write("interface TenGigE0/0/0/17." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/0/0/17." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/0/0/17." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')


# 10.222.128.50 

f.write("****************************************************************************************************************************" + '\n')
f.write("**************************************************** 10.222.128.50 *********************************************************" + '\n')
f.write("****************************************************************************************************************************" + '\n' + '\n'+ '\n')

for k in vlans:
	f.write("--------------------------------------  10.222.128.50  Vlan " + str(k) + " ------------------------------" + '\n' + "!" + '\n')
	f.write("interface TenGigE0/1/0/2." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/1/0/2." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/1/0/2." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')
	
	f.write("interface TenGigE0/1/0/3." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/1/0/3." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/1/0/3." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')

	f.write("interface TenGigE0/1/0/22." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/1/0/22." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/1/0/22." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')

	f.write("interface TenGigE0/1/0/23." + str(k)+ " l2transport" + '\n')
	f.write("interface TenGigE0/1/0/23." + str(k)+ " l2transport description . " + '\n')
	f.write("interface TenGigE0/1/0/23." + str(k)+ " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write('\n')


f.close()