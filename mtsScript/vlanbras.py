#!/ /usr/local/bin/python
# -*- coding: utf-8 -*-

# Этот скрипт создает конфиг для настройки новых вланов ( sub-interface ) на новых брасах в УЛьяновске 10.222.128.30 и 10.222.128.31

# Создадим список требуемых НЕЧЕТНЫХ вланов

vlan_odd = [541,543,545,547,549,551,553,555,557,559,561,563,565,567,569,671,673,675,677,679,681,683,685,687,689,691,693,695]

# Создадим список требуемых ЧЕТНЫХ вланов

vlan_even = [540,542,544,546,548,550,552,554,556,558,560,562,564,566,568,670,672,674,676,678,680,682,684,686,688,690,692,694,696]


# Создаем переменную f, связанную с файлом, куда будем писать наш конфиг
f = open("/dos/aka/Python/vlanbras.cfg",'w')

# 10.222.128.30  НЕчетные ODD

f.write("****************************************************************************************************************************" + '\n')
f.write("****************************************** 10.222.128.30  НЕчетные VLAN: ***************************************************" + '\n')
f.write("****************************************************************************************************************************" + '\n' + '\n'+ '\n')

for k in vlan_odd:
	f.write("--------------------------------------  10.222.128.30   Vlan " + str(k) + " ------------------------------" + '\n' + "!" + '\n')
	f.write("interface TenGigabitEthernet0/1/0." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/1/1." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/1/2." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/1/3." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')




# 10.222.128.30  четные EVEN

f.write("****************************************************************************************************************************" + '\n')
f.write("******************************************** 10.222.128.30  Четные VLAN: ***************************************************" + '\n')
f.write("****************************************************************************************************************************" + '\n' + '\n'+ '\n')

for k in vlan_even:
	f.write("--------------------------------------  10.222.128.30   Vlan " + str(k) + " ------------------------------" + '\n' + "!" + '\n')
	f.write("interface TenGigabitEthernet0/1/0." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/1/1." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/1/2." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/1/3." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')


# 10.222.128.31  НЕчетные odd

f.write("****************************************************************************************************************************" + '\n')
f.write("****************************************** 10.222.128.31  НЕчетные VLAN: ***************************************************" + '\n')
f.write("****************************************************************************************************************************" + '\n' + '\n'+ '\n')

for k in vlan_odd:
	f.write("--------------------------------------  10.222.128.31    Vlan " + str(k) + " ------------------------------" + '\n' + "!" + '\n')
	f.write("interface TenGigabitEthernet0/0/0." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/0/1." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/0/2." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/0/3." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

# 10.222.128.31 четные EVEN

f.write("****************************************************************************************************************************" + '\n')
f.write("******************************************** 10.222.128.31  Четные VLAN: ***************************************************" + '\n' )
f.write("****************************************************************************************************************************" + '\n' + '\n'+ '\n')

for k in vlan_even:
	f.write("--------------------------------------  10.222.128.31    Vlan " + str(k) + " ------------------------------" + '\n' + "!" + '\n')
	f.write("interface TenGigabitEthernet0/0/0." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/0/1." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/0/2." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

	f.write("interface TenGigabitEthernet0/0/3." + str(k)+'\n')
	f.write(" encapsulation dot1q " + str(k) + " second-dot1q any" + '\n')
	f.write(" ip verify unicast reverse-path" + '\n')
	f.write(" pppoe enable group MTS_B2C_2" + '\n')
	f.write(" punt-control enable" + '\n')
	f.write(" snmp trap ip verify drop-rate" + '\n')
	f.write("end" + '\n' + "!" +  '\n')

f.close()