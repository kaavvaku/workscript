#!/ /usr/local/bin/python
# -*- coding: utf-8 -*-

# создаем список, в котором храним все вланы
vlans = range(2,4095)

# создаем список, в котором храним активные вланы из порта Gi1/9. 
vlan_Gi_1_9 = [20,35,36,41,74,128,131,152,153,160,161,259,348,349,351,352,370,371,373,376,377,390,
	392,409,411,412,425,520,538,540,541,550,598,599,613,614,626,627,636,637,640,641,655,657,658,
	660,661,721,754,1017,1401,1509,1510,1594,1595,1600,1667,1900,1957]
# vlan_Gi_1_9.extend(range(80,83)) позволяет запихнуть в конец списка последовательность подряд идущих вланов c 80 по 82 ( не по 83!!!) 
vlan_Gi_1_9.extend(range(80,83))
vlan_Gi_1_9.extend(range(202,213))
vlan_Gi_1_9.extend(range(214,218))
vlan_Gi_1_9.extend(range(219,222))
vlan_Gi_1_9.extend(range(223,228))
vlan_Gi_1_9.extend(range(230,242))
vlan_Gi_1_9.extend(range(243,247))
vlan_Gi_1_9.extend(range(248,257))
vlan_Gi_1_9.extend(range(261,273))
vlan_Gi_1_9.extend(range(274,288))
vlan_Gi_1_9.extend(range(289,310))
vlan_Gi_1_9.extend(range(311,321))
vlan_Gi_1_9.extend(range(323,328))
vlan_Gi_1_9.extend(range(330,333))
vlan_Gi_1_9.extend(range(335,347))	
vlan_Gi_1_9.extend(range(354,363))
vlan_Gi_1_9.extend(range(365,369))
vlan_Gi_1_9.extend(range(380,389))
vlan_Gi_1_9.extend(range(394,408))
vlan_Gi_1_9.extend(range(415,424))
vlan_Gi_1_9.extend(range(430,443))
vlan_Gi_1_9.extend(range(450,456))
vlan_Gi_1_9.extend(range(463,466))
vlan_Gi_1_9.extend(range(543,547))
vlan_Gi_1_9.extend(range(620,625))
vlan_Gi_1_9.extend(range(629,635))
vlan_Gi_1_9.extend(range(643,647))
vlan_Gi_1_9.extend(range(648,654))
vlan_Gi_1_9.extend(range(902,909))
vlan_Gi_1_9.extend(range(1060,1063))
vlan_Gi_1_9.extend(range(3396,3400))
# теперь отсортируем содержимое списка по возрастанию
vlan_Gi_1_9.sort()

# создаем список, в котором храним активные вланы из порта Gi1/10
vlan_Gi_1_10 = [23,151,800,995,2007]

# создаем список, в котором храним активные вланы из порта Gi1/17
vlan_Gi_1_17 = [23,24,35,90,128,151,160,550,800,997,2005]

# создаем список, в котором храним активные вланы из порта Gi1/19
vlan_Gi_1_19 = [1,15,23,38,50,90,101,128,151,160,239,241,304,321,322,800,987,989,990,993,994,
	1001,2001,2008,2012,2013]
vlan_Gi_1_19.extend(range(997,1000))
vlan_Gi_1_19.extend(range(2003,2007))
vlan_Gi_1_19.extend(range(2015,2020))
vlan_Gi_1_19.sort()

# создаем список, в котором храним активные вланы из порта Gi1/24
vlan_Gi_1_24 = [23,35,90,151,454,463,620,997,2005]

# создаем список, в котором храним активные вланы из порта Gi1/26
vlan_Gi_1_26 = [200,201]

# создаем список, в котором храним активные вланы из порта Te-2/1
vlan_TE_2_1 = [23,24,35,151,800,994,2008]

# создаем список, в котором храним активные вланы из порта Te-2/6
vlan_TE_2_6 = [23,24,35,90,128,151,160,620,800,989,990,999,1000,1001,1060,1061,1401,1957,2002,2012,2013,2015,2016,2017,2019]

# создаем список, в котором храним активные вланы из порта Te-2/7
vlan_TE_2_7 = [23,24,35,90,128,151,800,905,1001,2001]

# создаем список, в котором храним активные вланы из порта Te-2/8
vlan_TE_2_8 = [23,35,90,151,455,908,1000,2002]

# создаем список, в котором храним активные вланы из порта Te-4/1
vlan_TE_4_1 = [1,23,24,62,74,90,128,151,153,160,344,395,409,465,541,544,598,599,613,614,620,
	626,627,636,637,655,657,658,660,661,800,991,993,996,1017,1062,1509,1600,1667,1900,2000,2006]
vlan_TE_4_1.extend(range(300,322))
vlan_TE_4_1.extend(range(622,625))
vlan_TE_4_1.extend(range(629,635))
vlan_TE_4_1.extend(range(640,647))
vlan_TE_4_1.extend(range(648,654))
vlan_TE_4_1.extend(range(2009,2012))
vlan_TE_4_1.extend(range(2020,2031))
vlan_TE_4_1.sort()

# создаем список, в котором храним активные вланы из порта Te-4/2
vlan_TE_4_2 = [1,23,24,62,74,90,128,151,153,160,344,395,409,465,541,544,598,599,613,614,620,
	626,627,636,637,655,657,658,660,661,800,991,993,996,1017,1062,1509,1600,1667,2000,2006]
vlan_TE_4_2.extend(range(300,322))
vlan_TE_4_2.extend(range(622,625))
vlan_TE_4_2.extend(range(629,635))
vlan_TE_4_2.extend(range(640,647))
vlan_TE_4_2.extend(range(648,654))
vlan_TE_4_2.extend(range(2009,2012))
vlan_TE_4_2.extend(range(2020,2031))
vlan_TE_4_2.sort()

# создаем список, в котором храним активные вланы из порта Te-4/3
vlan_TE_4_3 = [23,24,35,90,128,151,800,999,1595,2003]

# создаем список, в котором храним активные вланы из порта Te-4/4
vlan_TE_4_4 = [23,24,35,90,128,151,208,280,620,800,906,998,1510,2004]

# создаем список, в котором храним активные вланы из порта Te-4/5
vlan_TE_4_5 = [23,24,35,128,151,464,800,900,1594,2014]

# создаем список, в котором храним активные вланы из порта Te-4/6
vlan_TE_4_6 = [23,35,90,128,151,800,987,2018]

# создаем список, в котором храним активные вланы из порта Te-4/7
vlan_TE_4_7 = [90,200,538,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,3700]

# создаем список, в котором храним активные вланы из порта Po3
vlan_Po3 = [201,538,1017,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,3700,4050]

# создаем переменную f и привязываем ее к файлу asr9006_nvrsk.cfg, открываем этот файл на запись
f = open("asr9006_nvrsk.cfg",'w')

# в цикле создаем все BD со 2-4094
for k in vlans:
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) +'\n')
f.write('\n')

# заносим в файл строки с дескрипшенами для каждого порта
f.write("interface Te0/0/0/0 description [M][MBH]h1-nvrsk-mts" + '\n')
f.write("interface Te0/0/0/1 description [M][MU]sm1-nvrsk-kutuzovskaya117a" + '\n')
f.write("interface Te0/0/0/2 description [M][MBH]mts-ug1-link" + '\n')
f.write("interface Te0/0/0/3 description [M][MBH]mts-ug2-link" + '\n')
f.write("interface Te0/0/0/4 description [M][MU]d1-nvrsk-nabereg9-mkc" + '\n')
f.write("interface Te0/0/0/5 description manag_ASR_9006" + '\n')
f.write("interface Te0/0/0/6 description [M][MU]d2-nvrsk-nabereg9-mkc" + '\n')
f.write("interface Te0/0/0/7 description MCAST-nvrsk-MPLEX-to_ANAPA_via_MBH" + '\n')
f.write("interface Te0/0/0/8 description [M][MU]sm1-nvrsk-anapskoe41j-p4" + '\n')
f.write("interface Te0/0/0/9 description [M]to_MBH_PPPoE(13/2/2(2))" + '\n')
f.write("interface Te0/1/0/0 description [M]to_MBH_PPPoE(13/2/3(2))" + '\n')
f.write("interface Te0/1/0/1 description [M]to_MBH_PPPoE(13/2/6(2))" + '\n')
f.write("interface Te0/0/0/10 description [M][MU]d2-nvrsk-kozlova62" + '\n')
f.write("interface Te0/0/0/11 description [M][MU]d2-nvrsk-kunikova9-p4_new" + '\n')
f.write("interface Te0/0/0/12 description [M][MU]d3-nvrsk-kozlova62_new" + '\n')
f.write("interface Te0/0/0/13 description [M][MU]sm1-nvrsk-anp-krasno-zelenyh6" + '\n')
f.write("interface Te0/0/0/14 description [M][MU]sm1-nvrsk-anp-parkovaya62b" + '\n')
f.write("interface Te0/0/0/15 description [M][MU]d2-nvrsk-pionerskaya39-p1" + '\n')
f.write("interface Te0/0/0/16 description [M][MU]d1-nvrsk-suvorovskaya2b-p2" + '\n')
f.write("interface Te0/0/0/17 description [M][MU]d2-nvrsk-vidova167-p10" + '\n')
f.write("interface Te0/0/0/18 description [M][MU]sm1-nvrsk-desant65-1-p3" + '\n')
f.write("interface Te0/0/0/19 description [M][MBH]mbh_to_asr9000(8/0/0(1))" + '\n')
f.write("interface Te0/0/0/20 description [M][MBH]mbh_to_asr9000(13/2/4(2)->8/0/1(1))" + '\n')
f.write("interface Bundle-Ether3 description [M]to_MBH_30G_PPPoE" + '\n' + '\n')

# создаем sub-интерфейсы на порту Te0/0/0/0 для всех активных в этом порту вланов, и заносим их в соответствующий BD. 
for k in vlan_Gi_1_9:
	f.write("interface Te0/0/0/0." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/0." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/0." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/0." + str(k) + '\n' + '\n')

# Далее для всех портов тоже самое...
for k in vlan_Gi_1_10:
	f.write("interface Te0/0/0/1." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/1." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/1." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/1." + str(k) + '\n' + '\n')

for k in vlans:
	f.write("interface Te0/0/0/2." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/2." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/2." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/2." + str(k) + '\n' + '\n')

for k in vlans:
	f.write("interface Te0/0/0/3." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/3." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/3." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/3." + str(k) + '\n' + '\n')

for k in vlan_Gi_1_17:
	f.write("interface Te0/0/0/4." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/4." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/4." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/4." + str(k) + '\n' + '\n')
		
for k in vlan_Gi_1_19:
	f.write("interface Te0/0/0/5." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/5." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/5." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/5." + str(k) + '\n' + '\n')

for k in vlan_Gi_1_24:
	f.write("interface Te0/0/0/6." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/6." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/6." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/6." + str(k) + '\n' + '\n')

for k in vlan_Gi_1_26:
	f.write("interface Te0/0/0/7." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/7." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/7." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/7." + str(k) + '\n' + '\n')

for k in vlan_TE_2_1:
	f.write("interface Te0/0/0/8." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/8." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/8." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/8." + str(k) + '\n' + '\n')

for k in vlan_TE_2_6:
	f.write("interface Te0/0/0/10." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/10." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/10." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/10." + str(k) + '\n' + '\n')

for k in vlan_TE_2_7:
	f.write("interface Te0/0/0/11." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/11." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/11." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/11." + str(k) + '\n' + '\n')

for k in vlan_TE_2_8:
	f.write("interface Te0/0/0/12." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/12." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/12." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/12." + str(k) + '\n' + '\n')

for k in vlan_TE_4_1:
	f.write("interface Te0/0/0/13." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/13." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/13." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/13." + str(k) + '\n' + '\n')

for k in vlan_TE_4_2:
	f.write("interface Te0/0/0/14." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/14." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/14." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/14." + str(k) + '\n' + '\n')

for k in vlan_TE_4_3:
	f.write("interface Te0/0/0/15." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/15." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/15." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/15." + str(k) + '\n' + '\n')

for k in vlan_TE_4_4:
	f.write("interface Te0/0/0/16." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/16." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/16." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/16." + str(k) + '\n' + '\n')

for k in vlan_TE_4_5:
	f.write("interface Te0/0/0/17." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/17." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/17." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/17." + str(k) + '\n' + '\n')

for k in vlan_TE_4_6:
	f.write("interface Te0/0/0/18." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/18." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/18." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/18." + str(k) + '\n' + '\n')

for k in vlan_TE_4_7:
	f.write("interface Te0/0/0/19." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/19." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/19." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/19." + str(k) + '\n' + '\n')

for k in vlans:
	f.write("interface Te0/0/0/20." + str(k) + " l2transport" + '\n')
	f.write("interface Te0/0/0/20." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Te0/0/0/20." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Te0/0/0/20." + str(k) + '\n' + '\n')

for k in vlan_Po3:
	f.write("interface Bundle-Ether3." + str(k) + " l2transport" + '\n')
	f.write("interface Bundle-Ether3." + str(k) + " l2transport encapsulation dot1q " + str(k) + '\n')
	f.write("interface Bundle-Ether3." + str(k) + " l2transport rewrite ingress tag pop 1 symmetric" + '\n')
	f.write("l2vpn bridge group L2VPN bridge-domain VLAN" + str(k) + " interface Bundle-Ether3." + str(k) + '\n' + '\n')

# по окончании закрываем файл
f.close()
